const { Sequelize } = require("sequelize");
const config = require("../config/config");
const sequelize = new Sequelize(
  config.dataBase,
  config.userName,
  config.passWord,
  {
    host: config.host,
    port: config.port,
    dialect: config.dialect,
  }
);
module.exports = sequelize;

// yarn sequelize-auto -h localhost -d db_food_homework -u root -x 1234 -p 3308 --dialect mysql -o src/models -l es6
