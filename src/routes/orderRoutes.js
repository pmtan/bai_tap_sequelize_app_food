const express = require("express");
const orderRoutes = express.Router();
const {
  getOrders,
  getOrder,
  createOrder,
  updateOrder,
  deleteOrder,
} = require("../controllers/orderControllers");

orderRoutes.get("/getOrders", getOrders);
// orderRoutes.get("/getOrder", getOrder);
orderRoutes.post("/createOrder", createOrder);
// orderRoutes.put("/updateOrder", updateOrder);
orderRoutes.delete("/deleteOrder", deleteOrder);

module.exports = orderRoutes;
