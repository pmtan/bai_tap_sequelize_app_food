const express = require("express");
const {
  getRates,
  createRate,
  getRatesByUser,
  getRatesByRestaurant,
} = require("../controllers/rateControllers");
const rateRoutes = express.Router();

rateRoutes.get("/getRates", getRates);
rateRoutes.get("/getRatesByUser/:user_id", getRatesByUser);
rateRoutes.get("/getRatesByRestaurant/:res_id", getRatesByRestaurant);
rateRoutes.post("", createRate);

module.exports = rateRoutes;
