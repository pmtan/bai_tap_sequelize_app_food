const express = require("express");
const rootRoutes = express.Router();

// USER
const userRoutes = require("./userRoutes");
rootRoutes.use("/user", userRoutes);

// FOOD
const foodRoutes = require("./foodRoutes");
rootRoutes.use("/food", foodRoutes);

// LIKE
const likeRoutes = require("./likeRoutes");
rootRoutes.use("/like", likeRoutes);

// RESTAURANT
const restaurantRoutes = require("./restaurantRoutes");
rootRoutes.use("/restaurant", restaurantRoutes);

// SUBFOOD
const subFoodRoutes = require("./subFoodRoutes");
rootRoutes.use("/subFood", subFoodRoutes);

// ORDER
const orderRoutes = require("./orderRoutes");
rootRoutes.use("/order", orderRoutes);

// RATE
const rateRoutes = require("./rateRoutes");
rootRoutes.use("/rate", rateRoutes);

module.exports = rootRoutes;
