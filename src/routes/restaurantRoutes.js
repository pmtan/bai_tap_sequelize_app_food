const express = require("express");
const restaurantRoutes = express.Router();

const {
  getRestaurants,
  getRestaurant,
  createRestaurant,
  updateRestaurant,
  deleteRestaurant,
} = require("../controllers/restaurantControllers");

restaurantRoutes.get("/getRestaurants", getRestaurants);
restaurantRoutes.get("/getRestaurant/:res_id", getRestaurant);
restaurantRoutes.post("/createRestaurant", createRestaurant);
restaurantRoutes.put("/updateRestaurant/:res_id", updateRestaurant);
restaurantRoutes.delete("/deleteRestaurant/:res_id", deleteRestaurant);

module.exports = restaurantRoutes;
