const express = require("express");
const userRoutes = express.Router();

const {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  logIn,
  signUp,
} = require("../controllers/userControllers");

userRoutes.get("/getUsers", getUsers);
userRoutes.get("/getUser/:user_id", getUser);
userRoutes.post("/createUser", createUser);
userRoutes.post("/login", logIn);
userRoutes.post("/signup", signUp);
userRoutes.put("/updateUser/:user_id", updateUser);
userRoutes.delete("/deleteUser/:user_id", deleteUser);

module.exports = userRoutes;
