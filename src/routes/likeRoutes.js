const express = require("express");
const likeRoutes = express.Router();
const {
  getLikes,
  getLikesByUser,
  getLikesByRestaurant,
  createLike,
  deleteLike,
} = require("../controllers/likeControllers");

likeRoutes.get("/getLikes", getLikes);
likeRoutes.get("/getUserLikes/:user_id", getLikesByUser);
likeRoutes.get("/getRestaurantLikes/:res_id", getLikesByRestaurant);
likeRoutes.post("", createLike);
likeRoutes.delete("", deleteLike);
module.exports = likeRoutes;
