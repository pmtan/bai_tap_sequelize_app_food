const express = require("express");
const subFoodRoutes = express.Router();
const {
  getSubFoods,
  getSubFood,
  createSubFood,
  updateSubFood,
  deleteSubFood,
} = require("../controllers/subFoodControllers");

subFoodRoutes.get("/getSubFoods", getSubFoods);
subFoodRoutes.get("/getSubFood/:sub_id", getSubFood);
subFoodRoutes.post("/createSubFood", createSubFood);
subFoodRoutes.put("/updateSubFood/:sub_id", updateSubFood);
subFoodRoutes.delete("/deleteSubFood/:sub_id", deleteSubFood);

module.exports = subFoodRoutes;
