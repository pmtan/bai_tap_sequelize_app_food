const express = require("express");
const foodRoutes = express.Router();
const {
  getFoods,
  getFood,
  createFood,
  updateFood,
  deleteFood,
  uploadFoodPhoto,
} = require("../controllers/foodControllers");
const upload = require("../controllers/uploadControllers");

foodRoutes.get("/getFoods", getFoods);
foodRoutes.get("/getFood/:food_id", getFood);
foodRoutes.post("/createFood", createFood);
foodRoutes.post("/upload", upload.single("data"), uploadFoodPhoto);
foodRoutes.put("/updateFood/:food_id", updateFood);
foodRoutes.delete("/deleteFood/:food_id", deleteFood);

module.exports = foodRoutes;
