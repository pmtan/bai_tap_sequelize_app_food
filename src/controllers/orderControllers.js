const initModels = require("../models/init-models");
const sequelize = require("../models/connection");
const models = initModels(sequelize);
const { successCode, failCode, errorCode } = require("../config/response");
const { Op } = require("sequelize");
const getOrders = async (req, res) => {
  try {
    let data = await models.order.findAll();
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Loi Back end.");
  }
};

// const getOrder = async (req, res) => {
//   try {
//     let { user_id, food_id, amount } = req.body;
//     let data = await models.restaurant.findAll({
//       where: {
//         [Op.and]: [{ user_id }, { food_id }, { amount }],
//       },
//     });
//     successCode(res, data, "Lấy dữ liệu thành công.");
//   } catch (err) {
//     console.log(err);
//     errorCode(res, "Loi Back end.");
//   }
// };

const createOrder = async (req, res) => {
  try {
    let { user_id, food_id, amount, code, arr_sub_id } = req.body;
    let orderModel = {
      user_id,
      food_id,
      amount,
      code,
      arr_sub_id,
    };
    await models.order.create(orderModel);
    successCode(res, orderModel, "Tạo order mới thành công!");
  } catch (err) {
    errorCode(res, "Lỗi back end.");
  }
};

// const updateOrder = async (req, res) => {
//   try {
//     let { user_id, food_id, amount, code, arr_sub_id } = req.body;
//     let orderModel = {
//       user_id,
//       food_id,
//       amount,
//       code,
//       arr_sub_id,
//     };
//     let data = await models.order.update(orderModel, {
//       where: {
//         [Op.and]: [{ user_id }, { food_id }, { amount }],
//       },
//     });
//     successCode(res, data, "Update order thành công!");
//   } catch (err) {
//     errorCode(res, "Lỗi back end.");
//   }
// };
const deleteOrder = async (req, res) => {
  try {
    let { user_id, food_id, amount } = req.body;
    await models.order.destroy({
      where: {
        [Op.and]: [{ user_id }, { food_id }, { amount }],
      },
    });
    successCode(res, res_id, "Xoá order thành công!");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi back end.");
  }
};
module.exports = {
  getOrders,
  createOrder,
  deleteOrder,
};
