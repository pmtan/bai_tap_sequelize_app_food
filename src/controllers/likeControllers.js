const initModels = require("../models/init-models");
const sequelize = require("../models/connection");
const models = initModels(sequelize);
const { successCode, failCode, errorCode } = require("../config/response");
const { Op } = require("sequelize");

const getLikes = async (req, res) => {
  try {
    let data = await models.like_res.findAll();
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(data, " Lỗi BE.");
  }
};

const getLikesByUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let data = await models.like_res.findAll({
      where: {
        user_id,
      },
    });
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(data, " Lỗi BE.");
  }
};

const getLikesByRestaurant = async (req, res) => {
  try {
    let { res_id } = req.params;
    let data = await models.like_res.findAll({
      where: {
        res_id,
      },
    });
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(data, " Lỗi BE.");
  }
};
const createLike = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    let likeModel = {
      user_id,
      res_id,
      date_like: new Date(),
    };
    await models.like_res.create(likeModel);
    successCode(res, likeModel, "Thêm like thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi BE.");
  }
};
const deleteLike = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    await models.like_res.destroy({
      where: {
        [Op.and]: [{ user_id }, { res_id }],
      },
    });
    successCode(res, user_id, "Unliked thanh cong.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi BE.");
  }
};
module.exports = {
  getLikes,
  getLikesByUser,
  getLikesByRestaurant,
  createLike,
  deleteLike,
};
