const initModels = require("../models/init-models");
const sequelize = require("../models/connection");
const { successCode, failCode, errorCode } = require("../config/response");
const models = initModels(sequelize);
const fs = require("fs");
const getFoods = async (req, res) => {
  try {
    let data = await models.food.findAll();
    successCode(res, data, "Lấy dữ liệu thành công!");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi Back End.");
  }
};
const getFood = async (req, res) => {
  try {
    let { food_id } = req.params;
    await models.food.findAll({
      where: {
        food_id,
      },
    });
    successCode(res, data, "Lấy dữ liệu thành công!");
  } catch (err) {
    errorCode(res, "Lỗi back end.");
  }
};
const createFood = async (req, res) => {
  try {
    let { food_name, image, price, desc } = req.body;
    let foodModel = {
      food_name,
      image,
      price,
      desc,
    };
    await models.food.create(foodModel);
    successCode(res, foodModel, "Tạo món ăn mới thành công!");
  } catch (err) {
    errorCode(res, "Lỗi back end.");
  }
};

const updateFood = async (req, res) => {
  try {
    let { food_id } = req.params;
    let { food_name, image, price, desc } = req.body;
    let foodModel = {
      food_name,
      image,
      price,
      desc,
    };
    await models.food.update(foodModel, {
      where: {
        food_id,
      },
    });
    successCode(res, foodModel, "Update món ăn thành công!");
  } catch (err) {
    errorCode(res, "Lỗi back end.");
  }
};
const deleteFood = async (req, res) => {
  try {
    let { food_id } = req.params;
    await models.food.destroy({
      where: {
        food_id,
      },
    });
    successCode(res, food_id, "Xoá món ăn thành công!");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi back end.");
  }
};
const uploadFoodPhoto = async (req, res) => {
  try {
    const file = req.file;
    res.send(file);
    fs.readFile(
      process.cwd() + "/public/foodImages" + file.filename,
      (err, data) => {
        let fileName = `${Buffer.from(data.toString("base64"))}`;
        // fs.unlink(process.cwd() + "public/foodImages" + file.filename);
        res.send(fileName);
      }
    );
  } catch (err) {
    console.log(err);
  }
};
module.exports = {
  getFoods,
  getFood,
  createFood,
  updateFood,
  deleteFood,
  uploadFoodPhoto,
};
