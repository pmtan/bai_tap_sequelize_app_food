const sequelize = require("../models/connection");
const initModels = require("../models/init-models");
const { successCode, failCode, errorCode } = require("../config/response");
const models = initModels(sequelize);
const bcrypt = require("bcrypt");
const { generateToken, verifyToken, decodeToken } = require("../config/jwt.js");
const getUsers = async (req, res) => {
  try {
    let data = await models.user.findAll();
    successCode(res, data, "Lấy dữ liệu thành công!");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi back end.");
  }
};
const getUser = async (req, res) => {
  let { user_id } = req.params;
  let data = await models.user.findAll({
    where: {
      user_id,
    },
  });
  successCode(res, data, "Lấy dữ liệu thành công!");
};
const createUser = async (req, res) => {
  try {
    let { full_name, email, pass_word } = req.body;
    let userModel = {
      full_name,
      email,
      pass_word,
    };
    console.log(userModel);
    await models.user.create(userModel);
    successCode(res, userModel, "Tạo user mới thành công!");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi back end.");
  }
};
const logIn = async (req, res) => {
  try {
    let { email, pass_word } = req.body;
    let user = await models.user.findOne({
      where: {
        email,
      },
    });
    if (user) {
      let isValid = bcrypt.compareSync(pass_word, user.pass_word);
      if (isValid) {
        let newToken = generateToken({
          data: { ...user.dataValues, pass_word: "" },
        });
        successCode(res, newToken, "Log in thành công!");
      } else {
        errorCode(res, "Mật khẩu không đúng!");
      }
    } else {
      failCode(res, "", "Email không tồn tại!");
    }
  } catch (error) {}
};
const signUp = async (req, res) => {
  try {
    let { full_name, email, pass_word } = req.body;
    let checkEnail = await models.user.findOne({
      where: {
        email,
      },
    });
    if (checkEnail) {
      failCode(res, email, "Email đã tồn tại!");
    } else {
      let userModel = {
        full_name,
        email,
        pass_word,
      };
      console.log(userModel);
      await models.user.create(userModel);
      successCode(res, userModel, "Tạo user mới thành công!");
    }
  } catch (err) {
    errorCode(res, "Loi BE.");
  }
};
const updateUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let { full_name, email, pass_word } = req.body;
    let userModel = {
      full_name,
      email,
      pass_word,
    };
    await models.user.update(userModel, {
      where: {
        user_id,
      },
    });
    successCode(res, userModel, "Update user thành công!");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi back end.");
  }
};
const deleteUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    await models.user.destroy({
      where: {
        user_id,
      },
    });
    successCode(res, user_id, "Xoá user thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi BE.");
  }
};

module.exports = {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  logIn,
  signUp,
};
