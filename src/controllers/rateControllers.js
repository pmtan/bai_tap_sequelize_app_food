const initModels = require("../models/init-models");
const sequelize = require("../models/connection");
const { successCode, errorCode } = require("../config/response");
const models = initModels(sequelize);

const getRates = async (req, res) => {
  try {
    let data = await models.rate_res.findAll();
    successCode(res, data, "Lay du lieu thanh cong!");
  } catch (error) {
    console.log(error);
    errorCode(res, "Loi back end.");
  }
};

const createRate = async (req, res) => {
  try {
    let { user_id, res_id, amount, date_rate } = req.body;
    let rateModel = {
      user_id,
      res_id,
      amount,
      date_rate,
    };
    await models.rate_res.create(rateModel);
    successCode(res, rateModel, "Danh gia nha hang thanh cong!");
  } catch (error) {
    console.log(error);
    errorCode(res, "Loi back end.");
  }
};

const getRatesByUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let data = await models.rate_res.findAll({
      where: {
        user_id,
      },
    });
    successCode(res, data, "Lay du lieu thanh cong!");
  } catch (error) {
    console.log(error);
    errorCode(res, "Loi back end.");
  }
};

const getRatesByRestaurant = async (req, res) => {
  try {
    let { res_id } = req.params;
    let data = await models.rate_res.findAll({
      where: {
        res_id,
      },
    });
    successCode(res, data, "Lay du lieu thanh cong!");
  } catch (error) {
    console.log(error);
    errorCode(res, "Loi back end.");
  }
};
module.exports = {
  getRates,
  createRate,
  getRatesByUser,
  getRatesByRestaurant,
};
