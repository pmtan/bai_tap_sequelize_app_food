const initModels = require("../models/init-models");
const sequelize = require("../models/connection");
const models = initModels(sequelize);
const { successCode, errorCode, failCode } = require("../config/response");

const getSubFoods = async (req, res) => {
  try {
    let data = await models.sub_food.findAll();
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Loi BE.");
  }
};

const getSubFood = async (req, res) => {
  try {
    let { sub_id } = req.params;
    let data = await models.sub_food.findAll({
      where: {
        sub_id,
      },
    });
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Loi Back end.");
  }
};

const createSubFood = async (req, res) => {
  try {
    let { sub_name, sub_price } = req.body;
    let subFoodModel = {
      sub_name,
      sub_price,
    };
    await models.sub_food.create(subFoodModel);
    successCode(res, subFoodModel, "Tạo subfood mới thành công.");
  } catch (error) {
    console.log(err);
    errorCode(res, "Loi Back end.");
  }
};

const updateSubFood = async (req, res) => {
  try {
    let { sub_id } = req.params;
    let { sub_name, sub_price } = req.body;
    let subFoodModel = {
      sub_name,
      sub_price,
    };
    await models.sub_food.update(subFoodModel, {
      where: {
        sub_id,
      },
    });
    successCode(res, subFoodModel, "Update subfood thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Loi BE");
  }
};
const deleteSubFood = async (req, res) => {
  try {
    let { sub_id } = req.params;
    await models.sub_food.destroy({
      where: {
        sub_id,
      },
    });
    successCode(res, sub_id, "Xoá subfood thành công.");
  } catch (error) {
    console.log(err);
    errorCode(res, "Loi BE.");
  }
};

module.exports = {
  getSubFoods,
  getSubFood,
  createSubFood,
  updateSubFood,
  deleteSubFood,
};
