const initModels = require("../models/init-models");
const sequelize = require("../models/connection");
const models = initModels(sequelize);
const { successCode, failCode, errorCode } = require("../config/response");

const getRestaurants = async (req, res) => {
  try {
    let data = await models.restaurant.findAll();
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Loi Back end.");
  }
};

const getRestaurant = async (req, res) => {
  try {
    let { res_id } = req.params;
    let data = await models.restaurant.findAll({
      where: {
        res_id,
      },
    });
    successCode(res, data, "Lấy dữ liệu thành công.");
  } catch (err) {
    console.log(err);
    errorCode(res, "Loi Back end.");
  }
};

const createRestaurant = async (req, res) => {
  try {
    let { res_name, image, desc } = req.body;
    let restaurantModel = {
      res_name,
      image,
      desc,
    };
    await models.restaurant.create(restaurantModel);
    successCode(res, restaurantModel, "Tạo nhà hàng mới thành công!");
  } catch (err) {
    errorCode(res, "Lỗi back end.");
  }
};

const updateRestaurant = async (req, res) => {
  try {
    let { res_id } = req.params;
    let { res_name, image, desc } = req.body;
    let restaurantModel = {
      res_name,
      image,
      desc,
    };
    await models.restaurant.update(restaurantModel, {
      where: {
        res_id,
      },
    });
    successCode(res, restaurantModel, "Update nhà hàng thành công!");
  } catch (err) {
    errorCode(res, "Lỗi back end.");
  }
};
const deleteRestaurant = async (req, res) => {
  try {
    let { res_id } = req.params;
    await models.restaurant.destroy({
      where: {
        res_id,
      },
    });
    successCode(res, res_id, "Xoá nhà hàng thành công!");
  } catch (err) {
    console.log(err);
    errorCode(res, "Lỗi back end.");
  }
};
module.exports = {
  getRestaurants,
  getRestaurant,
  createRestaurant,
  updateRestaurant,
  deleteRestaurant,
};
