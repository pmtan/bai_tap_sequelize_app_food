require("dotenv").config();
module.exports = {
  dataBase: process.env.DATABASE,
  userName: process.env.USERNAME,
  passWord: process.env.PASSWORD,
  host: process.env.HOST,
  port: process.env.PORT,
  dialect: process.env.DIALECT,
};
