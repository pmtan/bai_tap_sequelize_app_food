const jwt = require("jsonwebtoken");

const generateToken = (data) => {
  let token = jwt.sign(data, "hidden", { expiresIn: "5m" });
  return token;
};

const verifyToken = (token) => {
  let isValidToken = jwt.verify(token, "hidden");
  return isValidToken;
};

const decodeToken = (token) => {
  let decoded = jwt.decode(token);
  return decoded;
};

module.exports = {
  generateToken,
  verifyToken,
  decodeToken,
};
