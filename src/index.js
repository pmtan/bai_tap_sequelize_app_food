const express = require("express");
const app = express();
const rootRoutes = require("./routes/rootRoutes");
app.use(express.json());
app.use(express.static("."));
app.use("/api", rootRoutes);
app.listen(8080);
